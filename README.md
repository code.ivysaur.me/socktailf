# socktailf

![](https://img.shields.io/badge/written%20in-C-blue)

A unix tool to circularly buffer stdin for a local socket.

`socktailf` reads incoming lines from stdin into a circular buffer. The circular buffer can be read at any time over a local socket, which is removed when `socktailf` exits. The socket path is printed to stdout at startup.

## Usage


```
Usage:
  socktailf [options]

Options:
  -d                         Write the current buffer to stdout upon exit
  --help                     Display help text
  -n, --lines %count         Set the number of lines to buffer (default 15)
  -q                         Supress display of path to socket file
  -v, --version              Display version number and exit
  -x                         Exit after failure to read from stdin (e.g. EOF)
```


## Changelog

2015-05-03 1.1.0
- Feature: `-d` option to write buffer on exit
- Feature: `-q` option to suppress buffer path
- Feature: `-v` option to display version number
- Add HISTORY, COPYRIGHT sections to man page
- More closely standardise descriptions between man page and `--help`
- Fix permissions issue when installing
- [⬇️ socktailf-1.1.0.tar.gz](dist-archive/socktailf-1.1.0.tar.gz) *(3.26 KiB)*


2015-04-30 1.0.0
- Initial release
- [⬇️ socktailf-1.0.0.tar.gz](dist-archive/socktailf-1.0.0.tar.gz) *(2.89 KiB)*

